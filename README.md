### Tumblr Mod 「 polls 」  remove blue styling

**About:** Tumblr introduced [polls](https://staff.tumblr.com/post/706805192704802816) in Jan 2023. They look fine on the dashboard, but on desktop themes, they take on a blue appearance that may not sit well with the general aesthetic.  
**Author:** HT (@&#x200A;glenthemes)  
**View on Tumblr:** [glenthemes.tumblr.com/post/708014819571302400/unblue-polls](https://glenthemes.tumblr.com/post/708014819571302400/unblue-polls)

#### How to use:

Place the following code before `</head>`:
```html
<!--✻✻✻✻✻✻  UN-BLUE POLLS by @glenthemes  ✻✻✻✻✻✻-->
<link href="//static.tumblr.com/gtjt4bo/mXBrpdj0n/unblue-polls.css" rel="stylesheet">
<style>
.poll-post {
    --Poll-Question-Font-Size: 16px;
    
    --Poll-Option-Background-Color: #fcfcfc;
    --Poll-Option-Corner-Rounding: 18px;
    --Poll-Option-Border-Size: 2px;
    --Poll-Option-Border-Color: #efefef;
    --Poll-Option-Padding: 15px;
    --Poll-Option-Font-Size: 13px;
    --Poll-Option-Spacing: 10px;
    --Poll-Option-Text-Color: #666666;
    
    --Poll-Option-HOVER-Border-Color: #cfdae4;
    --Poll-Option-HOVER-Background-Color: #ebf0f4;
    --Poll-Option-HOVER-Text-Color: #1a1b1d;
    --Poll-Option-HOVER-Speed: 0.4s;
}
</style>
```
